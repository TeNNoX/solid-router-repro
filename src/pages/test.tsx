import { Component, createEffect, createResource, Suspense } from 'solid-js';
import { A, useParams, useRouteData } from '@solidjs/router';
import type { AboutDataType } from './about.data';

export default function Test() {
  const params = useParams()
  createEffect(() => {
    console.log('params', { ...params });
  });

  const [deferredFoo] = createResource(
    () => params.foo,
    async (foo) => {
      console.log('Starting deferredFoo', foo);
      await new Promise<void>((resolve) => setTimeout(() => resolve(), 1000)); // HACK: defer datastream render to display spinner while loading
      console.log('Loaded deferredFoo');
      return foo;
    }
  );
  createEffect(() => {
    console.log('deferredFoo result', deferredFoo());
  });

  return (
    <Suspense fallback={<span>Outer suspense test</span>}>
      <section class="bg-pink-100 text-gray-700 p-8 flex flex-col gap-4">
        <h1 class="text-2xl font-bold">About</h1>

        <div class="mt-4">
          <pre>Loading preview for: {JSON.stringify(params, undefined, 4)}</pre>
        </div>

        <p>
          <Suspense fallback={<span>Inner suspense test</span>}>
            <pre>defered: {deferredFoo()}</pre>
          </Suspense>
        </p>

        <A href={`/${new Date().toISOString()}`}><strong>Navigate</strong></A>
      </section>
    </Suspense>
  );
}
